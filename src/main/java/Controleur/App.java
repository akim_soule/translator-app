package Controleur;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {

	private VBox root;

	@Override
	public void start(Stage primaryStage) throws Exception {
		root = FXMLLoader.load(getClass().getResource("../VueFXML.fxml"));
		Scene scene = new Scene(root);

		primaryStage.setResizable(true);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Translator app");
		primaryStage.show();
		
	}
	
	public static void main(String[] args) {
		
		Application.launch(args);

	}

}
