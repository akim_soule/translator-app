package Controleur;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.TranslationResult;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.animation.RotateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class ControleurFXML implements Initializable {

	@FXML
	private TextArea inputId;

	@FXML
	private Button aMoinsId;

	@FXML
	private TextArea outputId;

	@FXML
	private ChoiceBox<String> choiceBoxFromId;

	@FXML
	private ChoiceBox<String> choiceBoxToId;

	@FXML
	private ImageView flatToId;

	@FXML
	private BorderPane borderPaneTopId;

	@FXML
	private ImageView flatFromId;

	@FXML
	private Button buttonTraduireId;

	@FXML
	private Button aPlusId;

	private Text actualiserIcone;
	private Image imageFrance, imageEnglad;

	private String key = "QHQzcckCvC8eDbuvOpE52azWbcJYBYqXCID6PaangIYU";
	private String url = "https://api.us-south.language-translator.watson.cloud.ibm.com/instances/72c70c20-6d99-44f6-8711-90e380379896";
	private String version = "2020-06-10";

	private int minSize = 9;
	private int maxSize = 20;

	public void initialize(URL location, ResourceBundle resources) {

		actualiserIcone = GlyphsDude.createIcon(FontAwesomeIcon.REFRESH, "25px");

		borderPaneTopId.setCenter(actualiserIcone);

		actualiserIcone.setOnMouseEntered(new EventHandler<Event>() {
			
			public void handle(Event event) {
				actualiserIcone.setFill(Color.DARKGREY);
			}
		});

		actualiserIcone.setOnMouseExited(new EventHandler<Event>() {
			
			public void handle(Event event) {
				actualiserIcone.setFill(Color.BLACK);
			}
		});

		actualiserIcone.setOnMouseClicked(new EventHandler<Event>() {
			
			public void handle(Event event) {
				RotateTransition rt = new RotateTransition(Duration.millis(1500), actualiserIcone);
				rt.setByAngle(360);
				rt.setCycleCount(1);
				rt.play();
				inputId.setText("");
				outputId.setText("");

			}
		});

		imageFrance = new Image(getClass().getResourceAsStream("/flag_france.jpg"));
		flatFromId.setImage(imageFrance);
		flatFromId.setStyle("-fx-border-radius: 10 10 10 10; -fx-background-radius: 20px;");

		imageEnglad = new Image(getClass().getResourceAsStream("/flag_england.jpg"));
		flatToId.setImage(imageEnglad);
		flatToId.setStyle("-fx-border-radius: 10 10 10 10; -fx-background-radius: 20px;");

		List<String> list = new ArrayList<String>();
		list.add("Anglais");
		list.add("Français");

		ObservableList<String> obList = FXCollections.observableList(list);
		choiceBoxFromId.getItems().clear();
		choiceBoxFromId.setItems(obList);

		choiceBoxToId.getItems().clear();
		choiceBoxToId.setItems(obList);

		choiceBoxFromId.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (newValue != null) {
					if (newValue.equals("Anglais")) {
						flatFromId.setImage(imageEnglad);
					}else if (newValue.equals("Français")) {
						flatFromId.setImage(imageFrance);
					}
					
				}
			}
		});

		choiceBoxToId.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (newValue != null) {
					if (newValue.equals("Anglais")) {
						flatToId.setImage(imageEnglad);
					}else if (newValue.equals("Français")) {
						flatToId.setImage(imageFrance);
					}
				}
			}
		});

		choiceBoxFromId.setValue("Français");
		choiceBoxToId.setValue("Anglais");

	}

	@FXML
	private void buttonTraduireOnAction(ActionEvent event) {
		
		String input = inputId.getText();
		if (input != null && !input.isEmpty()) {
			RotateTransition rt = new RotateTransition(Duration.millis(15000), actualiserIcone);
			rt.setByAngle(360 * 5);
			rt.setCycleCount(10);
			rt.play();
			
			String language = "";
			String choix = choiceBoxToId.getSelectionModel().getSelectedItem();
			if (choix.equals("Français")) {
				language = "en-fr";
			}else if(choix.equals("Anglais")){
				language = "fr-en";
			}
			
			IamAuthenticator authenticator = new IamAuthenticator(key);
			LanguageTranslator languageTranslator = new LanguageTranslator(version, authenticator);
			languageTranslator.setServiceUrl(url);

			TranslateOptions translateOptions = new TranslateOptions.Builder().addText(input).modelId(language).build();

			TranslationResult result = languageTranslator.translate(translateOptions).execute().getResult();

			outputId.setText(result.getTranslations().get(0).getTranslation());
			rt.stop();
		}
	}

	@FXML
	private void aMoinsOnAction(ActionEvent event) {
		double taille = inputId.getFont().getSize();
		if (taille >= minSize) {
			--taille;
			inputId.setFont(Font.font("System", taille));
			outputId.setFont(Font.font("System", taille));
		}
	}

	@FXML
	private void aPlusIdOnAction(ActionEvent event) {
		double taille = inputId.getFont().getSize();
		if (taille <= maxSize) {
			++taille;
			inputId.setFont(Font.font("System", taille));
			outputId.setFont(Font.font("System", taille));
		}

	}

}
